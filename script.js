/*
Générateur de citations
Par Nicolas Troadec

Le programme est une suite d'event listener, enfermés dans des fonctions. Le but est de maîtriser le moment où ces événements peuvent être écoutés. La représentation visuelle du programme étant un faux "terminal", il fallait que l'appui sur certaines touches (1, 2), suivi de "Enter" provoque le déclenchement de nouvelles fonction, l'apparition et la disparition de certains éléments sur la page, et à la fin l'écoute de nouveaux événements.

*/

let arr7, i, j, k, buttonElt, citationElt, contentElt, choixElt, choix1Elt, accueilElt, boolean, h1Elt, choice1Elt, choice2Elt, choice3Elt, choice4Elt, choice5Elt, citationNumber, choixCitationElt, nombreCitationElt, numberCitation, citationBlock, retourAccueilButton, spanCitationElt, choiceButton, choiceButtonElt, choiceButtonArr, conditionAccueil, choixGenFormElt, choixGenElt, choixNombreCitationsElt, carretChoixElt, carretChoixElt1, spanElt, numberElt, citationSpanElt, disparaitreCitationsElt; //déclaration des variables du script

buttonElt = document.getElementById('button');
citationElt = document.getElementById('citation');
contentElt = document.getElementById('content');
choixElt = document.getElementById('choix');
choix1Elt = document.getElementById('choix1');
accueilElt = document.getElementById('accueil');
h1Elt = document.getElementById('titleGenerator');
choixCitationElt = document.getElementById('choixCitation');
nombreCitationElt = document.getElementById('nombreCitation');
citationBlock = document.getElementById('citationBlock');
retourAccueilButton = document.getElementById('retourPageAccueil');
choiceButton = document.getElementsByClassName('choixCitation');
choiceButtonElt = document.getElementsByTagName('li');
chargementBoxElt = document.getElementById('chargementBox');
choixGenElt = document.getElementById('choixGen');
choixNombreCitationsElt = document.getElementById('choixNombreCitations');
carretChoixElt = document.getElementById('carretChoix');
carretChoixElt1 = document.getElementById('carretChoix1');
spanElt = document.createElement('span');
numberElt = document.createElement('span');
//création de tous les éléments dont on aura besoin pour le script

arr7 = []; //création du tableau qui sera utile pour stocker les citations en constructions

window.onload = welcome(); //fonction à charger au chargement de la fenêtre

function welcome() {
    accueilElt.style.display = 'none';
    contentElt.style.display = 'none';
    nombreCitationElt.style.display = 'none';
    //disparition des trois blocs qui doivent rester invisibles pour le moment
    window.addEventListener('keypress', (e) => {
        let key = e.which || e.keyCode;
        if (key === 13) { //si l'utilisateur appuie sur "Enter"
            chargementBoxElt.style.display = 'none'; //on fait disparaître l'écran de chargement
            accueilElt.style.display = "flex"; //on fait apparaître l'écran d'accueil
            choixGenerateur()
        }
    });
}

function choixGenerateur() { //fonction permettant de choisir le générateur de citation 1 ou 2
    window.addEventListener('keypress', (e) => {
        let key = e.which || e.keyCode;
        if ((key === 49) || (key === 1)) { //L'utilisateur appuie sur 1
            carretChoixElt.style.display = 'none';
            spanElt.textContent = '1';
            choixGenElt.appendChild(spanElt);
            pressEnterAfterChoice(1); //on lance la fonction permettant d'écouter l'événement d'appui sur la touche "Enter" avec en paramètre la touche 1
        } else if ((key === 50) || (key === 2)) { //L'utilisateur appuie sur 2
            carretChoixElt.style.display = 'none';
            spanElt.textContent = '2';
            choixGenElt.appendChild(spanElt);
            pressEnterAfterChoice(2); //la touche "2" est en paramètre de la fonction permettant d'écouter l'appui sur "Enter"
        }
    });
}

function pressEnterAfterChoice(choice) { //fonction event listener pour la touche "Enter", menant aux générateurs de citations en fonction de l'argument "Choice"
    window.addEventListener('keypress', (e) => {
        let key = e.which || e.keyCode;
        if (key === 13) { //peu importe le choix du générateur, on écoute l'appui sur la touche "Enter"
            accueilElt.style.display = 'none';
            nombreCitationElt.style.display = 'flex'; //on fait apparaître l'écran permettant de choisir le nombre de citations qu'on veut
            if (choice === 1) { //on retrouve ici le paramètre de la fonction, entré au moment du choix du générateur
                h1Elt.textContent = 'Générateur 1';
                boolean = true; //ce booléen permettra de déterminer par la suite si on utilise le générateur 1 ou deux
            } else if (choice === 2) {
                h1Elt.textContent = 'Générateur 2';
                boolean = false;
            }
            choixNombreCitations() //dans les deux cas, on lance la fonction menant à la suite
        }
    });
}

function choixNombreCitations() { //cette fonction d'écouter le choix de l'utilisateur sur le nombre de citations qu'il souhaite voir afficher
    window.addEventListener('keypress', (e) => {
        let key = e.which || e.keycode;
        switch (key) {
            case 49 || 1:
                showNumber(1);
                break;
            case 50 || 2:
                showNumber(2);
                break;
            case 51 || 3:
                showNumber(3);
                break;
            case 52 || 4:
                showNumber(4);
                break;
            case 53 || 5:
                showNumber(5);
                break;
        }
    })
}

function showNumber(number) { // sert à afficher visuellement le numéro qu'a choisi l'utilisateur, entre 1 et 5 + event listener sur la touche "Enter"
    numberElt.textContent = number; //insère dans l'élément HTML le paramètre de la fonction
    choixNombreCitationsElt.appendChild(numberElt);
    window.addEventListener('keypress', (e) => {
        let key = e.which || e.keyCode;
        if (key === 13) {  //quand la  touche "Enter" est pressée, appel de la fonction "createSpanElt" avec en paramètre le nombre de citations choisi par l'utilisateur
            switch (number) {
                case 1:
                    createSpanElt(1);
                    break;
                case 2:
                    createSpanElt(2);
                    break;
                case 3:
                    createSpanElt(3);
                    break;
                case 4:
                    createSpanElt(4);
                    break;
                case 5:
                    createSpanElt(5);
                    break;
            }
        }
    });
}

function createSpanElt(numberCitation) {
    nombreCitationElt.style.display = 'none';
    contentElt.style.display = 'flex'; //on passe à l'écran suivant... 
    for (i = 0; i < numberCitation; i++) { //en fonction du nombre de citation choisi, on va créer un certain nombre span
        spanCitationElt = document.createElement('span');
        spanCitationElt.id = `citation${i}`;
        spanCitationElt.className = 'citationsSpanElt';
        spanCitationElt.style.color = 'green';
        citationBlock.appendChild(spanCitationElt);
        arr7.push(`citation${i}`);
    }
    GenererCitations()
}


function randomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

buttonElt.addEventListener('click', GenererCitations)

function GenererCitations() {
    arr7.forEach(function (citation) {
        citationElt = document.getElementById(citation);
        i = randomInt(0, 9);
        j = randomInt(0, 9);
        k = randomInt(0, 9);
        if (boolean === true) {
            return citationElt.textContent = arr1[i] + arr2[j] + arr3[k];
        } else {
            return citationElt.textContent = arr4[i] + arr5[j] + arr6[k];
        }
    })
}

retourAccueilButton.addEventListener('click', () => {
    document.location.reload(true); /* permet de recharger la fenêtre pour relancer la fonction welcome() */
});

citationSpanElt = document.getElementsByClassName('citationsSpanElt');
disparaitreCitationsElt = document.getElementById('disparaitreCitations')
disparaitreCitationsElt.addEventListener('click', function () {
for (i = 0; i < citationSpanElt.length; i++) {
    citationSpanElt[i].innerHTML = '';
}
})